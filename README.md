# Example Composite Gradle Build

This is example code for Stack Overflow question, [How do I add a java gradle project on my file system as a dependency to another local project?][1]. It is used in [this answer][2].

## Software Versions

The example was tested with the following software:

* Gradle 7.6
* Java 19.0.1
* Windows 10

## Structure

There are two "independent" Gradle projects: `project1` and `project2`.

* `project1` is a Java library. It was created with the following command:

    ```none
    gradle init --type java-library --test-framework junit-jupiter --project-name project1 --package sample.project1 --dsl kotlin
    ```

* `project2` is a Java application. It was created with the following command:

    ```none
    gradle init --type java-application --test-framework junit-jupiter --project-name project2 --package sample.project2 --dsl kotlin
    ```

In both cases, when prompted with:

>```none
>Generate build using new APIs and behavior (some features may change in the next minor release)? (default: no) [yes, no]
>```

It was answered, "no".

See the commit history for modifications made to the generated projects.

## Running Example

Execute the following sequence of commands:

```none
git clone https://gitlab.com/stack-overflow1/so-75045567.git
cd ./so-75045567/project2
./gradlew app:run --include-build ../project1 --console plain
```

Output of Gradle:

```none
> Task :app:processResources NO-SOURCE
> Task :project1:lib:compileJava
> Task :project1:lib:processResources NO-SOURCE
> Task :project1:lib:classes
> Task :project1:lib:jar
> Task :app:compileJava
> Task :app:classes

> Task :app:run
Hello, this is a composite build!

BUILD SUCCESSFUL in 1s
4 actionable tasks: 4 executed
```

Note the `--console plain` is not necessary. It's only included in order to see the output of each task, which shows that tasks from the included build were executed.



  [1]: https://stackoverflow.com/q/75045567/6395627
  [2]: https://stackoverflow.com/a/75102173/6395627